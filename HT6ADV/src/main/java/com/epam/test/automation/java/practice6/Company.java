package com.epam.test.automation.java.practice6;

public class Company {

    private Employee[] employees;

    public Company(Employee[] employees) {
        this.employees = employees;
    }

    private double giveEverybodyBonus(double companyBonus){
        double bonus = 0;
        for (Employee employee : employees) {
            bonus = employee.setBonus(companyBonus);
        }
        return bonus;
    }

    public double totalToPay(double bonus){
        double salary = 0;
        for (Employee employee : employees) {
            salary = salary + (employee.getSalary() + giveEverybodyBonus(bonus));
        }
        return salary;
    }

    public String nameMaxSalary(){
        double maxSalary = 0;
        String lastName = null;
        for (Employee employee : employees) {
            if (maxSalary < employee.getSalary()) {
                lastName = employee.getName();
            }
        }
        return lastName;
    }
}
