package com.epam.test.automation.java.practice6;


public class SalesPerson extends Employee{

    private int percent;

    public SalesPerson(String name, double salary, int percent) {
        super(name, salary);
        this.percent = percent;
    }

    @Override
    public double setBonus(double bonus) {
        if (percent < 0) throw new IllegalArgumentException();

        if (percent > 100 & percent < 200){ ;
            return bonus * 2;
        }
        if (percent >= 200){
            return bonus * 3;
        }
        return bonus;
    }
}
