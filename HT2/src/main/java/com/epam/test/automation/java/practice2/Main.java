package com.epam.test.automation.java.practice2;


public class Main {
    private Main() {

    }

    public static void main(String[] args) {

        System.out.println(task2(9));
    }

    public static int task1(int value) {
        int y = 0;
        if (value > 0) {
            while (value > 0) {
                int x = value % 10;
                if (x % 2 != 0)
                    y = y + x;
                value = value / 10;
            }
        } else {
            throw new IllegalArgumentException();
        }
        return y;
    }


    public static int task2(int value) {
        if (value <= 0) throw new IllegalArgumentException();

        int count = 0;
        int a;
        while (value > 0) {
            a = value % 2;
            if (a != 0)
                count++;
            value = value / 2;
        }
        return count;
    }

    public static int task3(int value) {
        if (value == 1)
            return 0;

        int firstValue = 0;
        int secondValue = 1;
        int sum = firstValue + secondValue;
        int temporary;

        if (value > 0) {
            for (int i = 2; i < value; i++) {
                temporary = secondValue;
                secondValue = firstValue + secondValue;
                firstValue = temporary;

                sum += secondValue;
            }
            return sum;
        } else {
            throw new IllegalArgumentException();
        }
    }
}

