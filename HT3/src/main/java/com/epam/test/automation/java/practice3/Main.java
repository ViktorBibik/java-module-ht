package com.epam.test.automation.java.practice3;


public class Main {

    private Main() {

    }

    /**
     * In a given array of integers nums swap values of the first and the last array elements,
     * the second and the penultimate etc., if the two exchanged values are even.
     */
    public static int[] task1(int[] array) {

        for (int i = 0, j = array.length - 1; i < array.length / 2; i++, j--) {
            if (array[i] % 2 == 0 && array[j] % 2 == 0) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        return array;
    }



    public static int task2(int[] array) {
        if (array.length < 2) return 0;
        if (array.length == 2 && array[0] == array[1]) return 1;

        int maxValue = array[0];
        int indexMaxValue = 0;
        int result = 0;

        for (int i = 0; i < array.length; i++) {
            if (maxValue < array[i]) {
                maxValue = array[i];
                indexMaxValue = i;
            }
            if (maxValue == array[i]) {
                result = i - indexMaxValue;
            }
        }

        return result;
    }

    /**
     * In a predetermined two-dimensional integer array (square matrix) matrix
     * insert 0 into elements to the left side of the main diagonal,
     * and 1 into elements to the right side of the diagonal.
     */
    public static int[][] task3(int[][] matrix) {
        if (matrix.length > matrix.length + 1)
            throw new UnsupportedOperationException();

        int[][] newMatrix = new int[matrix.length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i > j) {
                    newMatrix[i][j] = 0;
                } else if (i < j) {
                    newMatrix[i][j] = 1;
                } else {
                    newMatrix[i][j] = matrix[i][j];
                }
            }

        }
        return newMatrix;
    }

    private static int[][] createMultiDimensionalArray(int[][] matrix) {
        if (matrix.length > matrix.length + 1)
            throw new UnsupportedOperationException();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = i + (j + 3);
            }
        }
        return matrix;
    }

}
