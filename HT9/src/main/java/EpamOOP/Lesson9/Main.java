package EpamOOP.Lesson9;

import java.util.Arrays;

public class Main {
        public static void main(String[] args) {
            // create matrix with 4 rows 3 cols
            Matrix mtrx1 = new Matrix(4, 3);
            System.out.println("Matrix 1:\n" + mtrx1);
            // exceptions
            System.out.println("Matrix(r,c) Exceptions:");
            try {
                new Matrix(0, 1);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(1, 0);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(-1, 1);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(1, -1);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // create matrix from existing array
            double[][] data2 = { { 1, 7, 3, 6, 2, 9 }, { 5, 1, 3, 8, 2, 4 }, { 7, 3, 0, 4, 1, 0 }, { 5, 1, 9, 8, 2, 4 },
                    { 1, 7, 3, 8, 6, 2 } };
            Matrix mtrx2 = new Matrix(data2);
            System.out.println("Matrix 2:\n" + mtrx2);
            // exceptions
            System.out.println("Matrix(data) Exceptions:");
            try {
                new Matrix(null);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(new double[0][1]);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(new double[1][0]);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                new Matrix(new double[0][0]);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // getRows() method
            System.out.println("Matrix 1 rows: " + mtrx1.getRows());
            System.out.println("Matrix 2 rows: " + mtrx2.getRows());
            // getCols() method
            System.out.println("Matrix 1 cols: " + mtrx1.getCols());
            System.out.println("Matrix 2 cols: " + mtrx2.getCols());
            // getData() method
            double[][] getData1 = mtrx1.getData();
            System.out.println("Matrix 1 data: ");
            for (double[] row : getData1)
                System.out.println(Arrays.toString(row));
            double[][] getData2 = mtrx2.getData();
            System.out.println("Matrix 2 data: ");
            for (double[] row : getData2)
                System.out.println(Arrays.toString(row));
            // getValue(r,c) method
            System.out.println("Matrix 1 element[1,2]: " + mtrx1.getValue(1, 2));
            System.out.println("Matrix 2 element[4,5]: " + mtrx2.getValue(4, 5));
            // exceptions
            System.out.println("getValue() Exceptions:");
            try {
                System.out.println(mtrx1.getValue(10, 20));
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                System.out.println(mtrx1.getValue(-1, 0));
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                System.out.println(mtrx1.getValue(0, 20));
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                System.out.println(mtrx1.getValue(0, -1));
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // setValue(r,c,value) method
            System.out.println("Matrix 1 set [2][2] = 8");
            mtrx1.setValue(2, 2, 8);
            System.out.println(mtrx1);
            // exceptions
            System.out.println("setValue(r,c,value) Exceptions:");
            try {
                mtrx1.setValue(10, 20, 8);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                mtrx1.setValue(-1, 0, 8);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                mtrx1.setValue(0, 20, 8);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            try {
                mtrx1.setValue(0, -1, 8);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // Matrix operations
            Matrix mtrx3 = new Matrix(new double[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
            Matrix mtrx4 = new Matrix(new double[][] { { 9, 8, 7 }, { 6, 5, 4 }, { 3, 2, 1 } });
            System.out.println("Matrix 3:\n" + mtrx3);
            System.out.println("Matrix 4:\n" + mtrx4);
            // add(other) method
            mtrx3.add(mtrx4);
            System.out.println("Matrix 3 + Matrix 4:\n" + mtrx3);
            // exception
            System.out.println("add(other) Exceptions:");
            try {
                mtrx3.add(mtrx2);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // subtract(other) method
            mtrx3.subtract(mtrx4);
            System.out.println("Matrix 3 - Matrix 4:\n" + mtrx3);
            // exception
            System.out.println("subtract(other) Exceptions:");
            try {
                mtrx3.subtract(mtrx2);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
            // mult(other) method
            Matrix mtrx5 = new Matrix(new double[][] { { 1, 2 }, { 4, 5 }, { 7, 8 } });
            Matrix mtrx6 = new Matrix(new double[][] { { 9, 8, 7 }, { 6, 5, 4 } });
            System.out.println("Matrix 5:\n" + mtrx5);
            System.out.println("Matrix 6:\n" + mtrx6);
            mtrx5.multiply(mtrx6);
            System.out.println("Matrix 5 * Matrix 6:\n" + mtrx5);
            Matrix mtrx7 = new Matrix(new double[][] { { 1, 2 }, { 4, 5 }, { 7, 8 } });
            Matrix mtrx8 = new Matrix(new double[][] { { 9, 8, 7 }, { 6, 5, 4 } });
            System.out.println("Matrix 7:\n" + mtrx7);
            System.out.println("Matrix 8:\n" + mtrx8);
            mtrx8.multiply(mtrx7);
            System.out.println("Matrix 7 * Matrix 8:\n" + mtrx8);
            // exceptions
            System.out.println("multiply(other) Exceptions:");
            try {
                mtrx1.multiply(mtrx2);
            } catch (MatrixException e) {
                System.out.println(e.getMessage());
            }
        }
}
