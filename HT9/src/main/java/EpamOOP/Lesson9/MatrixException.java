package EpamOOP.Lesson9;

public class MatrixException extends RuntimeException {
    private static final long serialVersionUID = 936718834356381559L;

    public MatrixException(String string) {
        super(string);	}
}