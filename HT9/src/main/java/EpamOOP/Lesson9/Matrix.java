package EpamOOP.Lesson9;

public class Matrix {

    private double[][] data;


    /**
     * constructor
     * @param rows - number of rows
     * @param cols - number of columns
     * @throws MatrixException - if rows < 1 or cols < 1
     */
    public Matrix(int rows, int cols) {
        if (rows < 1)
            throw new MatrixException("There must be at least one row in the matrix, rows = " + rows);
        if (cols < 1)
            throw new MatrixException("The matrix must have at least one column, cols = " + cols);
        data = new double[rows][cols];
    }

    /**
     * constructor
     * @param data - 2D array
     * @throws MatrixException - if data.lebgth < 1 or data[0].length < 1 or data == null
     */
    public Matrix(double[][] data) {
        if (data == null)
            throw new MatrixException("Initial array must not be null");
        if (data.length < 1)
            throw new MatrixException("There must be at least one row in the matrix, rows = " + data.length);
        if (data[0].length < 1)
            throw new MatrixException("The matrix must have at least one column, cols = " + data[0].length);
        this.data = data;
    }

    public int getRows() {
        return data.length;
    }

    public int getCols() {
        return data[0].length;
    }

    public double[][] getData() {
        double[][] output = new double[data.length][data[0].length];
        for (int r = 0; r < data.length; r++)
            for (int c = 0; c < data[0].length; c++) {
                output[r][c] = data[r][c];
            }
        return output;
    }

    /**
     * set value in specified row and column
     * @param row - number of row
     * @param col - number of column
     * @param value - value to set
     * @throws MatrixException - if row >= getRows() or row < 0 or col >= getCols() or col < 0
     */
    public void setValue(int row, int col, double value) {
        if (row >= getRows() || row < 0)
            throw new MatrixException("Wrong row number = " + row + ", number of rows = " + getRows());
        if (col >= getCols() || col < 0)
            throw new MatrixException("Wrong column number = " + col + ", number of columns = " + getCols());
        data[row][col] = value;
    }

    /**
     * get value of specified row and column
     * @param row - number of row
     * @param col - number of column
     * @throws MatrixException - if row >= getRows() or row < 0 or col >= getCols() or col < 0
     */
    public double getValue(int row, int col) {
        if (row >= getRows() || row < 0)
            throw new MatrixException("Wrong row number = " + row + ", number of rows = " + getRows());
        if (col >= getCols() || col < 0)
            throw new MatrixException("Wrong column number = " + col + ", number of columns = " + getCols());
        return data[row][col];
    }

    /**
     * replaces this matrix with the result of its addition with other matrix
     * @param other - matrix to add
     * @throws MatrixException - if matrices are of different sizes
     */
    public Matrix add(Matrix other) {
        if (getCols() != other.getCols() || getRows() != other.getRows())
            throw new MatrixException("Unable to add matrices of different sizes " + getRows() + "x" + getCols()
                    + " and " + other.getRows() + "x" + other.getCols());
        for (int r = 0; r < getRows(); r++)
            for (int c = 0; c < getCols(); c++) {
                setValue(r, c, getValue(r, c) + other.getValue(r, c));
            }
        return this;
    }

    /**
     * replaces the value of a matrix with the result of subtracting another matrix from it
     * @param other - matrix to subtract
     * @throws MatrixException - if matrices are of different sizes
     */
    public Matrix subtract(Matrix other) {
        if (getCols() != other.getCols() || getRows() != other.getRows())
            throw new MatrixException("Unable to subtract matrices of different sizes " + getRows() + "x" + getCols()
                    + " and " + other.getRows() + "x" + other.getCols());
        for (int r = 0; r < getRows(); r++)
            for (int c = 0; c < getCols(); c++) {
                setValue(r, c, getValue(r, c) - other.getValue(r, c));
            }
        return this;
    }

    /**
     * replaces the value of a matrix with the result of multiplying it by another matrix
     * @param other - matrix to multiply
     * @throws MatrixException - if number of columns in this matrix != number of rows in other matrix
     */
    public Matrix multiply(Matrix other) {
        if (getCols() != other.getRows())
            throw new MatrixException("Number of columns in matrix A must be equal to number of rows in matrix B");
        double[][] result = new double[getRows()][other.getCols()];
        for (int r = 0; r < getRows(); r++)
            for (int c = 0; c < other.getCols(); c++)
                for (int p = 0; p < getCols(); p++)
                    result[r][c] += getValue(r, p) * other.getValue(p, c);
        data = result;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (int r = 0; r < getRows(); r++) {
            for (int c = 0; c < getCols(); c++) {
                output.append(getValue(r, c) + " ");
            }
            output.append("\n");
        }
        return output.toString();
    }
}
