package com.epam.test.automation.java.practice1;

public class Main {

    protected Main(){

    }

    public static int task1(int n) {
        if (n > 0) return n * n;
        if (n < 0) return Math.abs(n);
        else return 0;
    }

    public static int task2(int n) {
        int a;
        int maxInt = 0;
        int minInt = Integer.MAX_VALUE;
        int avgInt = 0;
        int sum = 0;

        while (n > 0) {
            a = n % 10;
            if (maxInt < a)
                maxInt = a;
            if (a < minInt)
                minInt = a;
            sum = sum+a;
            avgInt = sum - (maxInt+minInt);

            n = n / 10;
        }
        return Integer.parseInt(maxInt+""+avgInt+""+minInt);
    }

}
