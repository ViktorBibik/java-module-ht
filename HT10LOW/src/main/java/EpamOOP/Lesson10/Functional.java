package EpamOOP.Lesson10;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Functional {
    interface Task1 {
        List<String> operate(char c, List<String> stringList);
    }

    // ------------------------------------------
    interface Task2 {
        List<Integer> operate(List<String> stringList);
    }

    // ------------------------------------------
    interface Task3 {
        List<String> operate(List<String> stringList);
    }

    // ------------------------------------------
    interface Task4 {
        List<String> operate(int k, List<String> stringList);
    }

    // ------------------------------------------
    interface Task5 {
        List<String> operate(List<Integer> integerList);
    }

    public static void main(String[] args) {
        {
            Task1 task1 = (c, stringList) -> stringList
                    .stream().filter(string -> string.startsWith(Character.toString(c))
                            && string.endsWith(Character.toString(c)) && string.length() > 1)
                    .collect(Collectors.toList());

            char c = 'a';
            List<String> stringList = Arrays.asList("Hello", "qwerty", "asda", "asdfa", "as", "a");
            System.out.printf("Task 1 input: c = '%s' stringList = %s\n", c, stringList);
            System.out.println("Task 1 output:" + task1.operate(c, stringList));
        }
        // --------------------------------------
        {
            Task2 task2 = (stringList) -> stringList.stream().map(string -> string.length()).sorted()
                    .collect(Collectors.toList());
            List<String> stringList = Arrays.asList("Hello", "world", "!", "Good", "morning", "!");
            System.out.printf("Task 2 input: stringList = %s\n", stringList);
            System.out.println("Task 2 output: " + task2.operate(stringList));
        }
        // --------------------------------------
        {
            Task3 task3 = (stringList) -> stringList.stream()
                    .map(string -> string.charAt(0) + "" + string.charAt(string.length() - 1))
                    .collect(Collectors.toList());
            List<String> stringList = Arrays.asList("asd", "a", "basdw");
            System.out.printf("Task 3 input: stringList = %s\n", stringList);
            System.out.println("Task 3 output: " + task3.operate(stringList));
        }
        // --------------------------------------
        {
            Task4 task4 = (k, stringList) -> stringList.stream().filter(
                    string -> string.length() == k && "1234567890".contains(string.substring(string.length() - 1)))
                    .sorted().collect(Collectors.toList());
            int k = 2;
            List<String> stringList = Arrays.asList("8DC3", "4F", "B", "3S", "S3", "A1", "2A3G", "1B");
            System.out.printf("Task 4 input: k=%d stringList = %s\n", k, stringList);
            System.out.println("Task 4 output: " + task4.operate(k, stringList));
        }

        // --------------------------------------
        {
            Task5 task5 = (integerList) -> integerList.stream().filter(integer -> integer % 2 == 1)
                    .map(integer -> integer.toString()).sorted().collect(Collectors.toList());
            List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6);
            System.out.printf("Task 5 input: integerList = %s\n", integerList);
            System.out.println("Task 5 output: " + task5.operate(integerList));
        }
    }
}
