package EpamOOP.Lesson10;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Functional {
    // ------------------------------------------
    interface Task6 {
        List<String> operate(List<Integer> integerList, List<String> stringList);
    }

    // ------------------------------------------
    interface Task7 {
        List<Integer> operate(int k, List<Integer> integerList);
    }

    // ------------------------------------------
    interface Task8 {
        List<Integer> operate(int k, int d, List<Integer> integerList);
    }

    // ------------------------------------------
    interface Task9 {
        List<String> operate(List<String> stringList);
    }

    // ------------------------------------------
    interface Task10 {
        List<Character> operate(List<String> stringList);
    }
    public static void main(String[] args) {

        // --------------------------------------
        {
            Task6 task6 = (integerList, stringList) -> integerList.stream()
                    .map(n -> stringList.stream()
                            .filter(string -> string.length() == n && "1234567890".contains(string.substring(0, 1)))
                            .findFirst().orElse("Not Found"))
                    .collect(Collectors.toList());
            List<Integer> integerList = Arrays.asList(1, 3, 4);
            List<String> stringList = Arrays.asList("1aa", "aaa", "1", "a");
            System.out.printf("Task 6 input: integerList = %s stringList = %s\n", integerList, stringList);
            System.out.println("Task 6 output: " + task6.operate(integerList, stringList));
        }

        // --------------------------------------
        {
            Task7 task7 = (k, integerList) -> Stream
                    .concat(integerList.stream().filter(integer -> integer % 2 == 0), integerList.stream().skip(k))
                    .collect(Collectors.groupingBy(n -> n)).entrySet().stream()
                    .filter(entry -> entry.getValue().size() == 1).map(entry -> entry.getKey())
                    .sorted((a, b) -> Integer.compare(b, a)).collect(Collectors.toList());
            int k = 5;
            List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
            System.out.printf("Task 7 input: k=%d integerList = %s\n", k, integerList);
            System.out.println("Task 7 output: " + task7.operate(k, integerList));
        }
        // --------------------------------------
        {
            Task8 task8 = (k, d, integerList) -> Stream
                    .concat(integerList.stream().filter(integer -> integer > d), integerList.stream().skip(k))
                    .distinct().sorted((a, b) -> Integer.compare(b, a)).collect(Collectors.toList());
            int k = 4;
            int d = 3;
            List<Integer> integerList = Arrays.asList(-10, 3, -3, 4, 55, 6);
            System.out.printf("Task 8 input: k=%d d=%d integerList = %s\n", k, d, integerList);
            System.out.println("Task 8 output:" + task8.operate(k, d, integerList));
        }
        // --------------------------------------
        {
            Task9 task9 = (stringList) -> stringList.stream()
                    .collect(Collectors.groupingBy(string -> string.substring(0, 1))).entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey(),
                            e -> e.getValue().stream().map(string -> string.length()).reduce(0, Integer::sum)))
                    .entrySet().stream()
                    .sorted((a, b) -> Integer.compare(b.getValue(), a.getValue()) != 0
                            ? Integer.compare(b.getValue(), a.getValue())
                            : Integer.compare(a.getKey().codePointAt(0), b.getKey().codePointAt(0)))
                    .map(entry -> entry.getValue() + "-" + entry.getKey()).collect(Collectors.toList());
            List<String> stringList = Arrays.asList("ABC", "A", "BCD", "D");
            System.out.printf("Task 9 input: stringList = %s\n", stringList);
            System.out.println("Task 9 output: " + task9.operate(stringList));
        }

        // --------------------------------------
        {
            Task10 task10 = (stringList) -> stringList.stream()
                    .sorted((a, b) -> Integer.compare(b.length(), a.length()))
                    .map(string -> string.toUpperCase().charAt(string.length() - 1)).collect(Collectors.toList());
            List<String> stringList = Arrays.asList("asnsbiu", "asdsad", "asnsb", "asdf", "asdfb", "as", "a", "aop");
            System.out.printf("Task 10 input: stringList = %s\n", stringList);
            System.out.println("Task 10 output: " + task10.operate(stringList));
        }
    }
}
