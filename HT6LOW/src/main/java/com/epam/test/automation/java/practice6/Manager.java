package com.epam.test.automation.java.practice6;


public class Manager extends Employee {

    private int quantity;

    public Manager(String name, double salary, int clientAmount) {
        super(name, salary);
        quantity = clientAmount;
    }

    @Override
    public double setBonus(double bonus) {
        if (quantity < 0) throw new IllegalArgumentException();
        if (quantity > 100 & quantity <= 150) {
            return bonus + 500;
        }
        if(quantity > 150){
            return bonus + 1000;
        }
        return bonus;
    }
}
