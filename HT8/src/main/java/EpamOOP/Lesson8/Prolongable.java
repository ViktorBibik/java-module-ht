package EpamOOP.Lesson8;

public interface Prolongable {
    public boolean canToProlong();
}
