package EpamOOP.Lesson8;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit implements Prolongable {
    private int percent = 1;
    private double baseDeposit = getAmount();
    private BigDecimal incomeResult;

    public double getPercent() {
        return percent;
    }

    public SpecialDeposit(double depositAmount, int depositPeriod) {

        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        if (getPeriod() <= 0)
            throw new IllegalArgumentException();
        if (getPeriod() >= 1) {
            int percentGrow = percent;
            double depositGrow = baseDeposit;
            double income = 0;
            for (double i = 1; i <= getPeriod(); i++) {
                double depositIncome = (depositGrow * percentGrow) / 100;
                income += depositIncome;
                depositGrow += depositIncome;
                percentGrow++;
            }
            incomeResult = new BigDecimal(String.valueOf(income)).setScale(2, RoundingMode.DOWN);
        } else {
            throw new IllegalArgumentException();
        }
        return incomeResult;
    }

    @Override
    public boolean canToProlong() {
        return getAmount() > 1000;
    }

    @Override
    public String toString() {
        return "SpecDeposit{" + "percent=" + percent + ", period=" + getPeriod() + ", baseDeposit=" + getAmount()
                + ", income=" + income() + ", totalDepositAmount=" + new BigDecimal(getAmount()).add(income()) + '}';
    }

    @Override
    public int compareTo(Deposit o) {
        return this.income().intValue()-o.income().intValue();
    }
}
