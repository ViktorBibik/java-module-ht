package EpamOOP.Lesson8;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;

public class Client implements Iterator<Deposit> {
    private Deposit[] deposits;
    private int count;
    private BigDecimal totalIncome;
    private int iterator;

    public Client() {
        deposits = new Deposit[10];
        iterator = 0;
    }

    public boolean addDeposit(Deposit deposit) {
        if (count < deposits.length) {
            deposits[count] = deposit;
            count++;
            return true;
        }
        return false;
    }

    public BigDecimal totalIncome() {
        try {
            for (int i = 0; i < deposits.length - 1; i++) {
                BigDecimal income = deposits[0].income();
                totalIncome = income.add(deposits[i].income());
            }

        } catch (NullPointerException ne) {
            System.out.println("+++++");
        }
        return totalIncome;
    }

    public BigDecimal maxIncome() {
        BigDecimal maxIncome = deposits[0].income();
        try {
            for (int i = 0; i < deposits.length - 1; i++) {
                if (maxIncome.compareTo(deposits[i].income()) <= 0) {
                    maxIncome = deposits[i].income();
                }
            }
        } catch (NullPointerException e) {
            System.out.println("-------");
        }
        return maxIncome;
    }

    public BigDecimal getIncomeByNumber(int number) {
        if (number < 0)
            return BigDecimal.valueOf(0);
        if (number > deposits.length)
            return BigDecimal.valueOf(0);

        BigDecimal currentIncome = null;
        int lastFilledValueIndex = 0;
        for (int i = 0; i < deposits.length - 1; i++) {
            if (i == number) {
                currentIncome = deposits[i].income();
            }
        }
        return currentIncome;
    }

    public int countPossibleToProlongDeposit() {
        int count = 0;
        for (Deposit deposit : deposits)
            if (deposit != null && deposit instanceof Prolongable)
                if (((Prolongable) deposit).canToProlong())
                    count++;
        return count;
    }

    public void sortDeposits() {
        Arrays.sort(deposits, (a, b) -> a != null && b != null ? b.compareTo(a) : a == null ? 1 : -1);
        iterator = 0;
    }

    @Override
    public boolean hasNext() {

        return iterator < count;
    }

    @Override
    public Deposit next() {
        Deposit output;
        if (hasNext()) {
            output = deposits[iterator];
            iterator++;
        } else
            return null;

        return output;
    }

    @Override
    public String toString() {
        String output = "Client{" + "deposits=";
        for (Deposit deposit : deposits)
            if (deposit != null)
                output += deposit + " ";
        output += ", totalIncome=" + totalIncome() + ", count=" + count + '}';

        return output;
    }
}
