package EpamOOP.Lesson8;


public class Program {
    public static void main(String[] args) {
            Client client = new Client();
            System.out.println(client);
            Deposit base = new BaseDeposit(1000,48);
            System.out.println(base);
            Deposit special = new SpecialDeposit(1001,48);
            System.out.println(special);
            Deposit term = new LongDeposit(1000,36);
            System.out.println(term);
            client.addDeposit(base);
            client.addDeposit(special);
            client.addDeposit(term);
            System.out.println("possible to prolong deposit: "+client.countPossibleToProlongDeposit());
            System.out.println("unsorted deposits: "+client);
            client.sortDeposits();
            System.out.println("sorted deposits: "+client);
            System.out.println("deposits iteration:");
            while (client.hasNext()) {
                System.out.println(client.next());
            }
        }
}
