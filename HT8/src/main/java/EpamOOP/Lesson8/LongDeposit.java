package EpamOOP.Lesson8;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDeposit extends Deposit implements Prolongable {
    private final double PERCENT = 15.0;
    private double baseDeposit = getAmount();
    private BigDecimal incomeResult;

    public LongDeposit(double depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        if (getPeriod() <= 0)
            throw new IllegalArgumentException();

        if (getPeriod() <= 6)
            return BigDecimal.valueOf(getAmount());

        if (getPeriod() >= 7) {
            double income = 0;
            double depositGrow = baseDeposit;
            for (double i = 7; i <= getPeriod(); i++) {
                double depositIncome = (depositGrow * PERCENT) / 100; // percent of deposit
                income += depositIncome;
                depositGrow += depositIncome;
            }

            incomeResult = new BigDecimal(String.valueOf(income)).setScale(2, RoundingMode.DOWN);
        } else
            throw new IllegalArgumentException();
        return incomeResult;
    }

    @Override
    public boolean canToProlong() {
        return getPeriod() <= 36;
    }

    @Override
    public String toString() {
        return "LongDeposit{" + "percent=" + PERCENT + ", period=" + getPeriod() + ", baseDeposit=" + getAmount()
                + ", totalIncome=" + income() + ", totalDepositAmount=" + new BigDecimal(getAmount()).add(income())
                + '}';
    }

    @Override
    public int compareTo(Deposit o) {
        return this.income().intValue()-o.income().intValue();
    }
}
