package EpamOOP.Lesson7;

import java.math.BigDecimal;

public abstract class Deposit {
    private double amount;
    private int period;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Deposit(double depositAmount, int depositPeriod){
        amount = depositAmount;
        period = depositPeriod;
    }

    public abstract BigDecimal income();
}
