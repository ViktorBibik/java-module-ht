package EpamOOP.Lesson7;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit {
    private int percent = 1;
    private double baseDeposit = getAmount();
    private BigDecimal incomeResult;

    public double getPercent() {
        return percent;
    }

    public SpecialDeposit(double depositAmount, int depositPeriod) {

        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        if(getPeriod() <=0) throw new IllegalArgumentException();

        if (getPeriod() >= 1) {

            for (double i = 1; i <= getPeriod(); i++) {
                double depositIncome = (baseDeposit * getPercent()) / 100;
                baseDeposit = baseDeposit + depositIncome;
                percent++;
            }
            double income = baseDeposit - getAmount();
            incomeResult = new BigDecimal(String.valueOf(income)).setScale(2, RoundingMode.DOWN);
        }
        return incomeResult;
    }

    @Override
    public String toString() {
        return "SpecialDeposit{" +
                "percent=" + percent +
                ", totalPeriod=" + getPeriod() +
                ", baseDeposit=" + getAmount() +
                ", totalIncome=" + income() +
                ", depositIncome=" + baseDeposit +
                '}';
    }
}
