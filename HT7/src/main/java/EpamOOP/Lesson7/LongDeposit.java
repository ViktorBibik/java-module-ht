package EpamOOP.Lesson7;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDeposit extends Deposit {
    private final double PERCENT = 15.0;
    private double baseDeposit = getAmount();
    private BigDecimal incomeResult;

    public LongDeposit(double depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        if(getPeriod() <=0) throw new IllegalArgumentException();

        if (getPeriod() <= 6) return BigDecimal.valueOf(getAmount());

        if (getPeriod() >= 7) {
            for (double i = 7; i <= getPeriod(); i++) {
                double depositIncome = (baseDeposit * PERCENT) / 100; //percent of deposit
                baseDeposit = baseDeposit + depositIncome;
            }
            double income = baseDeposit - getAmount();
            incomeResult = new BigDecimal(String.valueOf(income)).setScale(2, RoundingMode.DOWN);
        }
        return incomeResult;
    }

    @Override
    public String toString() {
        return "LongDeposit{" +
                "PERCENT=" + PERCENT +
                ", totalPeriod=" + getPeriod() +
                ", baseDeposit=" + getAmount() +
                ", totalIncome=" + income() +
                '}';
    }
}
