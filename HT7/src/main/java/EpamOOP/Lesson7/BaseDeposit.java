package EpamOOP.Lesson7;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class BaseDeposit extends Deposit {

    private final double PERCENT = 5.0;
    private double depositIncome;
    private double income;
    private BigDecimal incomeResult;

    public BaseDeposit(double depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        depositIncome = getAmount() * (Math.pow(1.0 + (PERCENT / 100), getPeriod()));
        income = depositIncome - getAmount();
        incomeResult = new BigDecimal(String.valueOf(income)).setScale(2, RoundingMode.DOWN);
        return incomeResult;
    }

    @Override
    public String toString() {
        return "BaseDeposit{" +
                "PERCENT=" + PERCENT +
                ", totalPeriod=" + getPeriod() +
                ", baseDepositAmount=" + getAmount() +
                ", totalIncome=" + income() +
                ", totalDepositAmount=" + depositIncome +
                '}';
    }

}
