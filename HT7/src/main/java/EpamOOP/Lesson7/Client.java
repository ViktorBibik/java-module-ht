package EpamOOP.Lesson7;

import java.math.BigDecimal;

public class Client {
    private Deposit[] deposits;
    private int count;
    private BigDecimal totalIncome;

    public Client() {

        deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {
        if (count < deposits.length) {
            deposits[count] = deposit;
            count++;
            return true;
        }
        return false;
    }

    public BigDecimal totalIncome() {
        try {
            for (int i = 0; i < deposits.length - 1; i++) {
                BigDecimal income = deposits[0].income();
                totalIncome = income.add(deposits[i].income());
            }

        } catch (NullPointerException ne) {
            System.out.println("+++++");
        }
        return totalIncome;
    }

    public BigDecimal maxIncome() {
        BigDecimal maxIncome = deposits[0].income();
        try {
            for (int i = 0; i < deposits.length - 1; i++) {
                if (maxIncome.compareTo(deposits[i].income()) <= 0) {
                    maxIncome = deposits[i].income();
                }
            }

        } catch (NullPointerException e) {
            System.out.println("-------");
        }
        return maxIncome;
    }

    public BigDecimal getIncomeByNumber(int number) {
        if (number < 0)
            return BigDecimal.valueOf(0);
        if (number > deposits.length)
            return BigDecimal.valueOf(0);

        BigDecimal currentIncome = null;
        int lastFilledValueIndex = 0;
        for (int i = 0; i < deposits.length - 1; i++) {
            if (i == number) {
                currentIncome = deposits[i].income();
            }
        }
        return currentIncome;
    }

    @Override
    public String toString() {
        return "Client{" +
                //"deposits=" + Arrays.toString(deposits) +
                ", totalIncome=" + totalIncome() +
                ", count=" + count +
                '}';
    }
}
