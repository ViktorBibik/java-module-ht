package EpamOOP.Lesson7;


public class Program {
    public static void main(String[] args) {
        BaseDeposit db1 = new BaseDeposit(1000,6);
        LongDeposit ld1 = new LongDeposit(1000,7);
        SpecialDeposit sp1 = new SpecialDeposit(1000, 2);
        BaseDeposit db2 = new BaseDeposit(1000,7);
        BaseDeposit db3 = new BaseDeposit(1000,9);

        Client cl1 = new Client();

        cl1.addDeposit(db1);
        cl1.addDeposit(db3);
        cl1.addDeposit(ld1);
        cl1.addDeposit(sp1);
        cl1.addDeposit(db2);


//        System.out.println(cl1.maxIncome());
        System.out.println(cl1.getIncomeByNumber(4));
    }
}
