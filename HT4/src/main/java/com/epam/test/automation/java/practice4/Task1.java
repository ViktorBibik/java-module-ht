package com.epam.test.automation.java.practice4;


public class Task1 {

    /**
     * Create function IsSorted, determining whether a given array of integer
     * values of arbitrary length is sorted in a given order (the order is set up by
     * enum value SortOrder). Array and sort order are passed by parameters.
     * Function does not change the array.
     */
    public static boolean isSorted(int[] array, SortOrder order) {
        if (order == SortOrder.ASC){
            return isArraySortedAsc(array);
        }
                return isArraySortedDesc(array);
    }

    public static boolean isArraySortedAsc(int[] array) {
        if (array == null || array.length <= 1) {
            return true;
        }
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isArraySortedDesc(int[] array) {
        if (array == null || array.length <= 1) {
            return true;
        }
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                return false;
            }
        }
        return true;
    }
}


