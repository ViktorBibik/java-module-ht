package com.epam.test.automation.java.practice4;

public class Task3 {

    /**
     *Create function MultiArithmeticElements,
     * which determines the multiplication of the first n elements of arithmetic progression of real numbers
     * with a given initial element of progression a1 and progression step t an is calculated by the formula (an+1 = an + t).
     */
    public static int multiArithmeticElements(int a1, int t, int n) {
        int a = 1;
        int maxArrValue = a1 + t * (n - 1);
        for (int i = a1; i <= maxArrValue; i = i + t) {
            a = a * i;
        }
        return a;
    }
}
