package com.epam.test.automation.java.practice4;

import static com.epam.test.automation.java.practice4.Task1.isSorted;

public class Task2 {

    /**
     * Create function Transform, replacing the value of each element of an
     * integer array with the sum of this element value and its index, only if the
     * given array is sorted in the given order (the order is set up by enum value
     * SortOrder). Array and sort order are passed by parameters. To check, if
     * the array is sorted, the function IsSorted from the Task 1 is called.
     */

    public static int[] transform(int[] array,  SortOrder order) {
        int[] result = array;
        if(isSorted(result, order)){
            int sumArrValueAndIndex;
            for (int i = 0; i < result.length; i++) {
                sumArrValueAndIndex = result[i] + i;
                result[i] = sumArrValueAndIndex;
            }
        }
        return result;
    }
}
