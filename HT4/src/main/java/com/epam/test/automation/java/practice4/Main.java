package com.epam.test.automation.java.practice4;

import java.util.Arrays;

import static com.epam.test.automation.java.practice4.Task1.isSorted;
import static com.epam.test.automation.java.practice4.Task2.transform;
import static com.epam.test.automation.java.practice4.Task3.multiArithmeticElements;
import static com.epam.test.automation.java.practice4.Task4.sumGeometricElements;

public class Main {
    private Main(){}

    public static void main(String[] args) {
        int[] arr = {6, 5, 4, 3, 2, 1};
        int [] arr2 = {1,2,3,4,5,6};
        int [] arr3 = {5, 17, 24, 88, 33, 2};
        System.out.println(isSorted(arr, SortOrder.DESC));
        System.out.println(Arrays.toString(arr2));
        System.out.println(Arrays.toString(transform(arr3, SortOrder.DESC)));
        System.out.println(multiArithmeticElements(2,4,4));
        System.out.println(multiArithmeticElements(5, 3, 4));
        System.out.println(sumGeometricElements(100, 0.5, 20));

    }
}