package com.epam.test.automation.java.practice4;

public class Task4 {

    public static double sumGeometricElements(int a1, double t, int alim) {
        double a = 0.0;
        for (double i = a1; i > alim; i = i * t) {
            a = a + i;
        }
        return a;
    }
}
