package com.epam.test.automation.java.practice4;

public class TestsTask1 {

    public static boolean IsSorted(SortOrder se, int[] arr) {
        if (se == SortOrder.ASC) {
            return true;
        } else {
            return se == SortOrder.DESC;
        }
    }
}
