package com.epam.test.automation.java.practice5;

public class ArrayRectangles {
    private Rectangle[] rectangleArray;
    int count;

    public Rectangle[] getRectangleArray() {
        return rectangleArray;
    }

    public ArrayRectangles(int length) {
        rectangleArray = new Rectangle[length];
        count = 0;
    }

    public ArrayRectangles(Rectangle[] rectangles) {
        rectangleArray = rectangles;
        count = rectangles.length;
    }

    public boolean addRectangle(Rectangle rectangle) {
        if (count < rectangleArray.length) {
            rectangleArray[count] = rectangle;
            count++;
            return true;
        }
        return false;
    }

    public int numberMaxArea() {
        double recArea = 0;
        int maxArea = 0;
        for (int i = 0; i < rectangleArray.length; i++) {
            if (recArea < rectangleArray[i].area()) {
                recArea = rectangleArray[i].area();
                maxArea = i;
            }
        }
        return maxArea;
    }

    public int numberMinPerimeter() {
        Rectangle recPerimeter = rectangleArray[0];
        int minPerimeter = 0;
        for (int i = 0; i < rectangleArray.length; i++) {
            if (recPerimeter.perimeter() > rectangleArray[i].perimeter()) {
                recPerimeter = rectangleArray[i];
                minPerimeter = i;
            }
        }
        return minPerimeter;
    }

    public int numberSquares(){
        int number = 0;

        for (int i = 0; i < rectangleArray.length; i++) {
            if(rectangleArray[i].isSquare()){
                number = number + 1;
            }
        }
        return number;
    }
}
