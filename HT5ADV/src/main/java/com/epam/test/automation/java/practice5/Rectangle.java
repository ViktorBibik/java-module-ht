package com.epam.test.automation.java.practice5;

public class Rectangle {


    private double sideA;
    private double sideB;

    public Rectangle(double a, double b) {
        sideA = a;
        sideB = b;
    }

    public Rectangle(double a) {
        sideA = a;
        sideB = 5;
    }

    public Rectangle() {
        sideA = 4;
        sideB = 3;
    }

    public double getSideA() {

        return sideA;
    }

    public double getSideB() {

        return sideB;
    }

    public double area() {
        if (isSquare()) {
            return getSideA() * getSideA();
        }
        return getSideA() * getSideB();
    }

    public double perimeter() {
        if (isSquare()) {
            return 4 * getSideA();
        }
        return 2 * (getSideA() + getSideB());
    }

    public boolean isSquare() {

        return getSideA() == getSideB();
    }

    public static double[] replaceSides(double firstSide, double secondSide) {

        double[] arr = new double[2];
        arr[0] = secondSide;
        arr[1] = firstSide;
        return arr;
    }
}
